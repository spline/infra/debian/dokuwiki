# ITALIAN TRANSLATION OF DOKUWIKI'S PO-DEBCONF FILE.
# COPYRIGHT (C) 2010 THE DOKUWIKI'S COPYRIGHT HOLDER
# This file is distributed under the same license as the dokuwiki package.
#
# Vincenzo Campanella <vinz65@gmail.com>, 2010.
# Daniele Forsi <daniele@forsi.it>, 2013
#
msgid ""
msgstr ""
"Project-Id-Version: dokuwiki 0.0.20091225c-9\n"
"Report-Msgid-Bugs-To: dokuwiki@packages.debian.org\n"
"POT-Creation-Date: 2013-10-27 19:00+0100\n"
"PO-Revision-Date: 2013-11-16 20:21+0100\n"
"Last-Translator: Vincenzo Campanella <vinz65@gmail.com>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "lighttpd"
msgstr "lighttpd"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid "Web server(s) to configure automatically:"
msgstr "Server web da configurare automaticamente:"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"DokuWiki runs on any web server supporting PHP, but only listed web servers "
"can be configured automatically."
msgstr ""
"DokuWiki può essere eseguito su qualsiasi server web che supporti PHP, "
"tuttavia è possibile configurare automaticamente solo i server web elencati."

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"Please select the web server(s) that should be configured automatically for "
"DokuWiki."
msgstr ""
"Selezionare i server web che dovranno essere configurati automaticamente per "
"DokuWiki."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should the web server(s) be restarted now?"
msgstr "Riavviare ora i server web?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"In order to activate the new configuration, the reconfigured web server(s) "
"have to be restarted."
msgstr ""
"Al fine di attivare la nuova configurazione, i server web riconfigurati "
"devono essere riavviati."

#. Type: string
#. Description
#: ../templates:3001
msgid "Wiki location:"
msgstr "Posizione del Wiki:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Specify the directory below the server's document root from which DokuWiki "
"should be accessible."
msgstr ""
"Specificare la directory sotto la radice dei documenti del server da cui "
"DokuWiki dovrà essere accessibile."

#. Type: select
#. Description
#: ../templates:4001
msgid "Authorized network:"
msgstr "Rete autorizzata:"

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"Wikis normally provide open access to their content, allowing anyone to "
"modify it. Alternatively, access can be restricted by IP address."
msgstr ""
"Normalmente i Wiki consentono il libero accesso al loro contenuto e "
"permettono a chiunque di modificarlo. Alternativamente è possibile "
"restringere l'accesso per indirizzo IP."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"If you select \"localhost only\", only people on the local host (the machine "
"the wiki is running on) will be able to connect. \"local network\" will "
"allow people on machines in a local network (which you will need to specify) "
"to talk to the wiki. \"global\" will allow anyone, anywhere, to connect to "
"the wiki."
msgstr ""
"Se si sceglie «solo host locale», la connessione sarà consentita solo "
"dall'host locale (la macchina su cui il Wiki è in esecuzione). Se si sceglie "
"«rete locale», l'accesso sarà consentito agli utenti delle macchine che si "
"trovano nella rete locale (che dovrà essere specificata). Se invece si "
"sceglie «globale», l'accesso sarà consentito a chiunque da qualsiasi luogo."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"The default is for site security, but more permissive settings should be "
"safe unless you have a particular need for privacy."
msgstr ""
"Il valore predefinito è per motivi di sicurezza del sito, ma anche delle "
"impostazioni più permissive dovrebbero essere sicure, a meno di avere un "
"bisogno particolare di riservatezza."

#. Type: select
#. Choices
#: ../templates:4002
msgid "localhost only"
msgstr "solo host locale"

#. Type: select
#. Choices
#: ../templates:4002
msgid "local network"
msgstr "rete locale"

#. Type: select
#. Choices
#: ../templates:4002
msgid "global"
msgstr "globale"

#. Type: string
#. Description
#: ../templates:5001
msgid "Local network:"
msgstr "Rete locale:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"The specification of your local network should either be an IP network in "
"CIDR format (x.x.x.x/y) or a domain specification (like .example.com)."
msgstr ""
"La rete locale deve essere specificata o come rete IP in formato CIDR (x.x.x."
"x/y), o come nome di dominio (come .example.com)."

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Anyone who matches this specification will be given full and complete access "
"to DokuWiki's content."
msgstr ""
"Chiunque corrisponda al valore specificato otterrà l'accesso completo e "
"totale al contenuto di DokuWiki."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Purge pages on package removal?"
msgstr "Eliminare le pagine alla rimozione del pacchetto?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"By default, DokuWiki stores all its pages in a file database in /var/lib/"
"dokuwiki."
msgstr ""
"In modo predefinito DokuWiki memorizza tutte le pagine in un file di "
"database che si trova in «/var/lib/dokuwiki»."

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Accepting this option will leave you with a tidier system when the DokuWiki "
"package is removed, but may cause information loss if you have an "
"operational wiki that gets removed."
msgstr ""
"Se si accetta questa opzione si disporrà di un sistema più ordinato in caso "
"di rimozione del pacchetto DokuWiki, ma si potrebbe anche avere una perdita "
"d'informazioni se si ha un Wiki produttivo che viene rimosso."

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Make the configuration web-writeable?"
msgstr "Rendere la configurazione scrivibile dal web?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"DokuWiki includes a web-based configuration interface. To be usable, it "
"requires the web server to have write permission to the configuration "
"directory."
msgstr ""
"DokuWiki include un'interfaccia di configurazione basata sul web. Affinché "
"sia possibile utilizzarla, è necessario fornire al server web i permessi di "
"scrittura sulla directory della configurazione."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"Accepting this option will give the web server write permissions on the "
"configuration directory and files."
msgstr ""
"Se si accetta questa opzione si daranno al server web i permessi di "
"scrittura sulla directory e sui file di configurazione."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"The configuration files will still be readable and editable by hand "
"regardless of whether or not you accept this option."
msgstr ""
"Indipendentemente dall'accettazione di questa opzione, i file di "
"configurazione saranno comunque modificabili manualmente e leggibili."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Make the plugins directory web-writeable?"
msgstr "Rendere la directory dei plugin scrivibile dal web?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"DokuWiki includes a web-based plugin installation interface. To be usable, "
"it requires the web server to have write permission to the plugins directory."
msgstr ""
"DokuWiki include un'interfaccia d'installazione dei plugin basata sul web. "
"Affinché sia possibile utilizzarla, è necessario fornire al server web i "
"permessi di scrittura sulla directory dei plugin."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Accepting this option will give the web server write permissions to the "
"plugins directory."
msgstr ""
"Se si accetta questa opzione si daranno al server web i permessi di "
"scrittura sulla directory dei plugin."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Plugins can still be installed by hand regardless of whether or not you "
"accept this option."
msgstr ""
"Indipendentemente dall'accettazione di questa opzione, è sempre possibile "
"installare manualmente i plugin."

#. Type: string
#. Description
#: ../templates:9001
msgid "Wiki title:"
msgstr "Titolo del Wiki:"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"The wiki title will be displayed in the upper right corner of the default "
"template and on the browser window title."
msgstr ""
"Il titolo del Wiki verrà mostrato nell'angolo in alto a destra del modello "
"predefinito e nel titolo della finestra del browser."

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC0 \"No Rights Reserved\""
msgstr "CC0 \"Nessun diritto riservato\""

# Uso le traduzioni dei nomi delle licenze da http://creativecommons.org/licenses/
#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution"
msgstr "CC Attribuzione"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-ShareAlike"
msgstr "CC Attribuzione - Condividi allo stesso modo"

#. Type: select
#. Choices
#: ../templates:10001
msgid "GNU Free Documentation Licence"
msgstr "GNU Free Documentation License"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial"
msgstr "CC Attribuzione - Non commerciale"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial-ShareAlike"
msgstr "CC Attribuzione - Non commerciale - Condividi allo stesso modo"

#. Type: select
#. Description
#: ../templates:10002
#| msgid "Wiki location:"
msgid "Wiki license:"
msgstr "Licenza del Wiki:"

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Please choose the license you want to apply to your wiki content. If none of "
"these licenses suits your needs, you will be able to add your own to the "
"file /etc/dokuwiki/license.php and to refer it in the main configuration "
"file /etc/dokuwiki/local.php when the installation is finished."
msgstr ""
"Scegliere la licenza da applicare al contenuto del Wiki. Se nessuna delle "
"licenze è adatta, se ne potrà aggiungere una al file «/etc/dokuwiki/license."
"php» e fare riferimento ad essa nel file principale di configurazione «/etc/"
"dokuwiki/local.php» quando l'installazione è terminata."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Creative Commons \"No Rights Reserved\" is designed to waive as many rights "
"as legally possible."
msgstr ""
"Creative Commons \"Nessun diritto riservato\" è strutturata per rinunciare "
"al maggior numero legalmente possibile di diritti."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution is a permissive license that only requires licensees to give "
"credit to the author."
msgstr ""
"CC Attribuzione è una licenza permissiva che richiede ai licenziatari solo "
"di citare l'autore."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-ShareAlike and GNU Free Documentation License are copyleft-"
"based free licenses (requiring modifications to be released under similar "
"terms)."
msgstr ""
"CC Attribuzione - Condividi allo stesso modo e GNU Free Documentation "
"License sono licenze libere basate sul copyleft (richiedono che le modifiche "
"siano rilasciate con condizioni simili)."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-NonCommercial and CC Attribution-Noncommercial-ShareAlike are "
"non-free licenses, in that they forbid commercial use."
msgstr ""
"CC Attribuzione - Non commerciale e CC Attribuzione - Non commerciale - "
"Condividi allo stesso modo sono licenze non libere in quanto proibiscono "
"l'uso commerciale."

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Enable ACL?"
msgstr "Abilitare ACL?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"Enable this to use an Access Control List for restricting what the users of "
"your wiki may do."
msgstr ""
"Accettare questa opzione per utilizzare un ACL (Access Control List) per "
"restringere i permessi degli utenti del Wiki."

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"This is a recommended setting because without ACL support you will not have "
"access to the administration features of DokuWiki."
msgstr ""
"È un'impostazione consigliata, in quanto senza il supporto di ACL non si "
"avrà l'accesso alle funzionalità di amministrazione di DokuWiki."

#. Type: string
#. Description
#: ../templates:12001
msgid "Administrator username:"
msgstr "Nome utente dell'amministratore:"

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"Please enter a name for the administrator account, which will be able to "
"manage DokuWiki's configuration and create new wiki users. The username "
"should be composed of lowercase ASCII letters only."
msgstr ""
"Inserire un nome utente per l'account dell'amministratore, il quale potrà "
"gestire la configurazione di DokuWiki e creare nuovi utenti del Wiki. Il "
"nome utente deve essere composto solo da caratteri ASCII minuscoli."

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"If this field is left blank, no administrator account will be created now."
msgstr ""
"Se questo campo viene lasciato vuoto non verrà creato, al momento, alcun "
"account di amministratore."

#. Type: string
#. Description
#: ../templates:13001
msgid "Administrator real name:"
msgstr "Nome reale dell'amministratore:"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"Please enter the full name associated with the wiki administrator account. "
"This name will be stored in the wiki password file as an informative field, "
"and will be displayed with the wiki page changes made by the administrator "
"account."
msgstr ""
"Inserire il nome completo associato all'account dell'amministratore del "
"Wiki. Questo nome verrà memorizzato nel file delle password del Wiki come "
"campo informativo e verrà mostrato nelle pagine Wiki modificate dall'account "
"dell'amministratore."

#. Type: string
#. Description
#: ../templates:14001
msgid "Administrator email address:"
msgstr "Indirizzo email dell'amministratore:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"Please enter the email address associated with the wiki administrator "
"account. This address will be stored in the wiki password file, and may be "
"used to get a new administrator password if you lose the original."
msgstr ""
"Inserire l'indirizzo di posta elettronica associato all'account "
"dell'amministratore del Wiki. Questo indirizzo verrà memorizzato nel file "
"delle password del Wiki e potrà essere utilizzato per ottenere una nuova "
"password dell'amministratore in caso di smarrimento dell'originale."

#. Type: password
#. Description
#: ../templates:15001
msgid "Administrator password:"
msgstr "Password dell'amministratore:"

#. Type: password
#. Description
#: ../templates:15001
msgid "Please choose a password for the wiki administrator."
msgstr "Scegliere una password per l'amministratore del Wiki."

#. Type: password
#. Description
#: ../templates:16001
msgid "Re-enter password to verify:"
msgstr "Inserire nuovamente la password per verifica:"

#. Type: password
#. Description
#: ../templates:16001
msgid ""
"Please enter the same \"admin\" password again to verify you have typed it "
"correctly."
msgstr ""
"Inserire nuovamente la medesima password dell'amministratore, per poter "
"verificare che sia stata digitata correttamente."

#. Type: error
#. Description
#: ../templates:17001
msgid "Password input error"
msgstr "Errore d'immissione della password"

#. Type: error
#. Description
#: ../templates:17001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Le due password inserite non sono identiche. Riprovare."

#. Type: select
#. Description
#: ../templates:18001
msgid "Initial ACL policy:"
msgstr "Politica iniziale di ACL:"

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"Please select what initial ACL configuration should be set up to match the "
"intended usage of this wiki:\n"
" \"open\":   both readable and writeable for anonymous users;\n"
" \"public\": readable for anonymous users, writeable for registered users;\n"
" \"closed\": readable and writeable for registered users only."
msgstr ""
"Scegliere la configurazione iniziale di ACL che dovrà essere impostata per "
"l'uso del Wiki:\n"
" «aperta»:   gli utenti anonimi possono leggere e scrivere;\n"
" «pubblica»: gli utenti anonimi possono solo leggere, gli utenti registrati "
"possono anche scrivere;\n"
" «chiusa»:   solo gli utenti registrati possono leggere e scrivere."

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"This is only an initial setup; you will be able to adjust the ACL rules "
"later."
msgstr ""
"Si tratta solo di un'impostazione iniziale; le regole di ACL possono "
"comunque essere modificate in un secondo tempo."

#. Type: select
#. Choices
#: ../templates:18002
msgid "open"
msgstr "aperta"

#. Type: select
#. Choices
#: ../templates:18002
msgid "public"
msgstr "pubblica"

#. Type: select
#. Choices
#: ../templates:18002
msgid "closed"
msgstr "chiusa"
