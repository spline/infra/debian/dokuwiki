#! /bin/sh
set -e

# Function to cleanup a wiki data
# (see <https://www.dokuwiki.org/tips:maintenance>)
cleanup() {
    # $1 ... full path to data directory of wiki
    # $2 ... number of days after which old files are to be removed
    # $3 ... whether or not to remove old revision files
    data_path="$1"
    max_days="$2"
    remove_attic="$3"

    cd "$data_path"
	
    # Purge files older than $max_days days from attic and media_attic (old revisions)
    if [ "$remove_attic" = "true" ]
    then
        find attic media_attic -type f -mtime "+$max_days" -delete
    fi
    
    # remove stale lock files (files which are 1-2 days old)
    find locks -name '*.lock' -type f -mtime +1 -delete
    
    # remove empty directories
    find attic cache index locks media media_attic media_meta \
         meta pages tmp -mindepth 1 -type d -empty -delete
    
    # remove files older than $max_days days from the cache
    find cache -type f -mtime "+$max_days" -delete

    cd - > /dev/null
}

# Function to update the spam blacklist
# (see <https://www.dokuwiki.org/blacklist>)
update_blacklist() {
    temp="$(tempfile -p doku -s blist)"
    trap "rm -f -- '$temp'" EXIT
    wget -q -O - 'http://meta.wikimedia.org/wiki/Spam_blacklist?action=raw' | grep -vF '<pre>' > "$temp"
    mv "$temp" "/etc/dokuwiki/wordblock.local.conf"
    trap - EXIT
}

# Get configuration
. /etc/default/dokuwiki
# Set variable default values
run_cleanup="${RUN_CLEANUP:-true}"
cleanup_maxdays="${CLEANUP_MAXDAYS:-180}"
remove_attic="${REMOVE_REVISIONS:-false}"
update_blacklist="${UPDATE_BLACKLIST:-false}"

# If configured, run the purge for files olders than specified (~6 months by
# default)
if [ "$run_cleanup" = "true" ]
then
    cd /var/lib/dokuwiki
    for data_dir in data farm/*/data
    do
        # Skip non-existent directories ("farm/*/data" in the case it matched
        # no directory!)
        if [ -d "$data_dir" ]
        then
            cleanup "$data_dir" "$cleanup_maxdays" "$remove_attic"
        fi
    done
    cd - > /dev/null
fi

# If configured, update the spam blacklist
if [ "$update_blacklist" = "true" ] && [ -x /usr/bin/wget ]
then
    update_blacklist
fi
