Source: dokuwiki
Section: web
Priority: optional
Maintainer: Debian DokuWiki Team <team+dokuwiki@tracker.debian.org>
Uploaders: Axel Beckert <abe@debian.org>,
           Anton Gladky <gladk@debian.org>
Build-Depends: debhelper-compat (= 13),
               po-debconf,
               slimit
Standards-Version: 4.6.1
Homepage: https://www.dokuwiki.org/
Vcs-Git: https://salsa.debian.org/debian/dokuwiki.git
Vcs-Browser: https://salsa.debian.org/debian/dokuwiki
Rules-Requires-Root: binary-targets

Package: dokuwiki
Architecture: all
Depends: javascript-common,
         libjs-jquery,
         libjs-jquery-cookie,
         libjs-jquery-ui,
         libphp-simplepie,
         php (>= 1:7.2),
         php-geshi,
         php-phpseclib (>= 2),
         php-random-compat,
         php-xml,
         ucf,
         ${misc:Depends},
         ${perl:Depends}
Recommends: imagemagick | php-gd,
            php-cli,
            php-ldap | php-mysql | php-pgsql,
            wget
Suggests: libapache2-mod-xsendfile
Description: standards compliant simple to use wiki
 DokuWiki is a wiki mainly aimed at creating documentation of any kind.
 It is targeted at developer teams, workgroups and small companies. It
 has a simple but powerful syntax which makes sure the datafiles remain
 readable outside the wiki and eases the creation of structured texts.
 All data is stored in plain text files -- no database is required.
